module.exports = {
    plugins: [
      'i18n',
    ],
    rules: {
      'i18n/no-non-english-text': 'error',
    },
};